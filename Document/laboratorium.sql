﻿# Host: localhost  (Version 5.5.5-10.4.10-MariaDB)
# Date: 2022-08-26 22:56:46
# Generator: MySQL-Front 6.1  (Build 1.11)


#
# Structure for table "alat"
#

DROP TABLE IF EXISTS `alat`;
CREATE TABLE `alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `jenis_alat_id` int(11) NOT NULL DEFAULT 0,
  `stok` int(11) DEFAULT 0,
  `foto` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "alat"
#

INSERT INTO `alat` VALUES (1,'Lenovo M300',3,20,'1658561144551959.jpg','2022-07-23 14:25:44','2022-07-23 15:16:22'),(2,'',0,0,NULL,'2022-07-23 14:25:56',NULL),(3,'HP Keyboard Gaming',1,15,'1658563210Capture.PNG','2022-07-23 15:00:10','2022-07-23 15:17:17');

#
# Structure for table "alat_keluar"
#

DROP TABLE IF EXISTS `alat_keluar`;
CREATE TABLE `alat_keluar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alat_id` int(11) NOT NULL DEFAULT 0,
  `jumlah` int(11) DEFAULT NULL,
  `keperluan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `user_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Data for table "alat_keluar"
#

INSERT INTO `alat_keluar` VALUES (3,20220612,0,'2','0000-00-00 00:00:00',NULL,2),(5,20220706,0,'5','0000-00-00 00:00:00',NULL,2),(6,20220707,0,'5','0000-00-00 00:00:00',NULL,1),(7,3,12,'dipinjem','2022-07-23 17:45:42',NULL,1);

#
# Structure for table "alat_masuk"
#

DROP TABLE IF EXISTS `alat_masuk`;
CREATE TABLE `alat_masuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alat_id` int(11) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT current_timestamp(),
  `stok_before` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `stok_after` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

#
# Data for table "alat_masuk"
#

INSERT INTO `alat_masuk` VALUES (1,1,'2022-07-23 11:33:47',1,9,10,1,'2022-07-23 15:47:11'),(2,0,'2022-07-23 11:33:47',NULL,NULL,NULL,NULL,NULL),(3,0,'2022-07-23 11:33:47',NULL,NULL,NULL,NULL,NULL),(4,0,'2022-07-23 11:33:47',NULL,NULL,NULL,NULL,NULL),(5,0,'2022-07-23 11:33:47',NULL,NULL,NULL,NULL,NULL),(6,0,'2022-07-23 11:33:47',NULL,NULL,NULL,NULL,NULL),(8,NULL,'2022-07-23 15:14:16',0,20,20,1,NULL),(9,NULL,'2022-07-23 15:14:51',0,20,20,1,NULL),(10,1,'2022-07-23 15:16:22',10,10,20,1,'2022-07-23 15:47:46'),(11,3,'2022-07-23 15:17:03',0,12,12,1,NULL),(12,3,'2022-07-23 15:17:17',12,3,15,1,NULL);

#
# Structure for table "jenis_alat"
#

DROP TABLE IF EXISTS `jenis_alat`;
CREATE TABLE `jenis_alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Data for table "jenis_alat"
#

INSERT INTO `jenis_alat` VALUES (1,'Keyboard'),(2,'Mouse'),(3,'Monitor'),(4,'Power Supply'),(5,'VGA');

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,'admin','202cb962ac59075b964b07152d234b70','admin@gmail.com','2022-06-12 07:07:42','2022-06-12 07:07:42',1),(2,'aminah','90b74c589f46e8f3a484082d659308bd','aminah@gmail.com','2022-06-12 07:07:44','2022-06-12 07:07:44',0),(5,'ilham','202cb962ac59075b964b07152d234b70','ilham@gmail.com','2022-07-06 14:37:51',NULL,1);
