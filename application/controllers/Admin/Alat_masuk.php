<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alat_masuk extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->title = 'Alat Masuk';
		$this->table = 'alat_masuk';

		$this->load->model('M_Master');

		if (!$this->session->userdata('user')) {
			$this->M_Master->warning('Silahkan login terlebih dahulu');
			redirect('login');
		}
	}

	public function index() {
        $select = "{$this->table}.*, alat.nama as nama_alat, users.username as username, jenis_alat.nama as jenis_alat";
        $data['data'] = $this->M_Master->get_join_id(
            $this->table,
            array(
                array(
                    'table' => 'alat',
                    'fk'    => $this->table . '.alat_id=alat.id'
                ),
				array(
                    'table' => 'users',
                    'fk'    => $this->table . '.user_id=users.id'
                ),
				array(
                    'table' => 'jenis_alat',
                    'fk'    => 'alat.jenis_alat_id=jenis_alat.id'
                ),
            ),
            null,
		$select)->result();
		$data['title'] = $this->title;
		$data['view'] = $this->table.'/index';

		$this->load->view('template/index', $data);
	}

	public function form($id = null) {
		if ($this->input->method(TRUE) == 'POST') {

			$alat_id 		= $this->input->post('alat_id');
			$stok_before 	= $this->input->post('stok_before');
			$jumlah 		= $this->input->post('jumlah');
			$stok_after 	= $this->input->post('stok_after');
			
			$data   = [
				'alat_id' 		=> $alat_id,
				'user_id'		=> $this->session->userdata('user')->id,
				'stok_before' 	=> $stok_before,
				'jumlah' 		=> $jumlah,
				'stok_after' 	=> $stok_after,
			];

            $msg    = 'Berhasil tambah data';
            if (!empty($id)) {
                $where  = ['id' => $id];
                $edit   = $this->M_Master->edit($this->table, $data, $where);
                $msg    = 'Berhasil ubah data';
            } else {
                $add    = $this->M_Master->add($this->table, $data);
				$this->M_Master->edit('alat', array('stok' => $stok_after), array('id' => $alat_id));
            }

            $this->M_Master->success($msg);
            redirect('admin/alat-masuk');
		}

		$data['detail'] = $id ? $this->M_Master->get_id($this->table, ['id' => $id])->row() : null;

		if($id){
			$alat_id = $data['detail']->alat_id;
			$jenis_alat_id = $this->M_Master->get_id('alat', ['id' => $alat_id])->row();
			$data['detail']->jenis_alat_id = $jenis_alat_id->jenis_alat_id; 
		}

		$data['jenis_alat']     = $this->M_Master->get('jenis_alat','id asc')->result();
		$jenis_alat_first 		= (!empty($data['jenis_alat'][0]->id) ? $data['jenis_alat'][0]->id : 0);
		$select 				= "alat.*, jenis_alat.nama as jenis_alat";
		$data['alat'] = $this->M_Master->get_join_id(
			'alat',
			array(
				array(
					'table' => 'jenis_alat',
					'fk'    => 'alat.jenis_alat_id=jenis_alat.id'
				),
			),
			array('jenis_alat_id' => $jenis_alat_first),
			$select)->result();
		
		
		$data['title'] = $this->title;
		$data['view'] = $this->table.'/form';

		$this->load->view('template/index', $data);
	}

	public function delete($id)
	{
		$where  = ['id' => $id];
		$del    = $this->M_Master->del($this->table, $where);
		$this->M_Master->success('Berhasil hapus data');
		
		redirect('admin/alat-keluar');
	}
}
