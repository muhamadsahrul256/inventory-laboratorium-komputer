<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alat extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->title = 'Alat';
        $this->table = 'alat';

        $this->load->model('M_Master');

		if (!$this->session->userdata('user')) {
			$this->M_Master->warning('Silahkan login terlebih dahulu');
			redirect('login');
		}
    }

    public function index()
    {
        $select = "{$this->table}.*, jenis_alat.nama as jenis_alat";
        $data['data'] = $this->M_Master->get_join_id(
            $this->table,
            array(
                array(
                    'table' => 'jenis_alat',
                    'fk'    => $this->table . '.jenis_alat_id=jenis_alat.id'
                ),
            ),
            null,
            $select)->result();
        $data['title'] = $this->title;
        $data['view'] = $this->table . '/index';

        $this->load->view('template/index', $data);
    }

    public function form($id = null)
    {
        if ($this->input->method(TRUE) == 'POST') {

            $jenis_alat   = $this->input->post('jenis_alat');
            $nama           = $this->input->post('nama');

            $data   = [
                'jenis_alat_id'     => $jenis_alat,
                'nama'              => $nama,
            ];

            $msg    = 'Berhasil tambah data';
            
            if ($_FILES['foto']['name']) {
                $new_name = time() . $_FILES['foto']['name'];
                $config['file_name'] = $new_name;
                $config['upload_path'] = './public/foto/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);

                create_folder(FCPATH . str_replace('./', '', $config['upload_path']));
                $data['foto'] = $new_name;
                if (!$this->upload->do_upload('foto')) {
                    $error = array('error' => $this->upload->display_errors());

                    $this->M_Master->warning(implode('<br>', $error));
                    $id = $id ? $id : '';
                    redirect('admin/alat/form/' . $id);
                } else {
                    $upload_data = array('upload_data' => $this->upload->data());
                }
            }

            if (!empty($id)) {

                $where  = ['id' => $id];
                $detail = $this->M_Master->get_id($this->table, $where)->row();
                $edit   = $this->M_Master->edit($this->table, $data, $where);
                $msg    = 'Berhasil ubah data';
            
            } else {
                $add    = $this->M_Master->add($this->table, $data);
            }

            $this->M_Master->success($msg);
            redirect('admin/alat');
        }

        $data['jenis_alat']     = $this->M_Master->get('jenis_alat')->result();
        $data['detail']         = $id ? $this->M_Master->get_id($this->table, ['id' => $id])->row() : null;
        $data['title']          = $this->title;
        $data['view']           = $this->table . '/form';

        $this->load->view('template/index', $data);
    }

    public function delete($id)
    {
        $where  = ['id' => $id];
        $del    = $this->M_Master->del($this->table, $where);
        $this->M_Master->success('Berhasil hapus data');

        redirect('admin/alat');
    }

    public function ajax_get_alat_list()
    {
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        $param = $this->input->get('jenis_alat_id');
        $conditions = array();
        if(!empty($param)){
            $conditions['jenis_alat_id'] = $param;
        }
        // echo "<pre>"; print_r($conditions); exit; 
        $select = "{$this->table}.*, jenis_alat.nama as jenis_alat";
        $return = $this->M_Master->get_join_id(
            $this->table,
            array(
                array(
                    'table' => 'jenis_alat',
                    'fk'    => $this->table . '.jenis_alat_id=jenis_alat.id'
                ),
            ),
            $conditions,
            $select)->result();
        
        $this->output->set_content_type('application/json');
        echo json_encode($return);
        exit;
    }

    public function ajax_get_alat_detail()
    {
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        $id = $this->input->get('alat_id');
        $return = $this->M_Master->get_id($this->table, ['id' => $id])->row();
        
        $this->output->set_content_type('application/json');
        echo json_encode($return);
        exit;
    }
}
