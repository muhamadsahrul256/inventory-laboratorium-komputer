<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->model('M_Master');
	}

	public function index()
	{
		if (!$this->session->userdata('user')) {
			$this->M_Master->warning('Silahkan login terlebih dahulu');
			redirect('login');
		}
	}

	public function login() {
        if ($this->input->method(TRUE) == 'POST') {
			$email = $this->input->post('email');
			$password = md5($this->input->post('password'));

			$where = [
				'email' => $email,
				'password' => $password,
			];

			$cek = $this->M_Master->get_id('users', $where)->row();

			if ($cek) {
				$this->session->set_userdata('user', $cek);
				$this->M_Master->success('Anda berhasil login');
				redirect('admin/alat');	
			} else {
				$this->M_Master->warning('Email atau password salah');
				redirect('login');
			}
        }

		$this->load->view('template/login');
	}

	public function register()
	{
        if ($this->input->method(TRUE) == 'POST') {
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = md5($this->input->post('password'));

			$where = "username = '$username' or email = '$email'";

			$cek = $this->M_Master->get_id('users', $where)->row();

			if (!$cek) {
                $add = $this->M_Master->add('users', [
					'username' => $username,
					'email' => $email,
					'password' => $password,
					'status' => 1,
					'created_at' => date('Y-m-d H:i:s'),
				]);
				$this->M_Master->success('Anda berhasil register');
				redirect('login');
			} else {
				$this->M_Master->warning('Username atau email sudah terdaftar');
				redirect('register');
			}
        }

		$this->load->view('template/register');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

}