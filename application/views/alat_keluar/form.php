<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Form <?=$detail ? 'Ubah' : 'Tambah'?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Jenis Alat</label>
                        <select name="jenis_alat" id="jenis_alat" class="form-control">
                            <?php foreach ($jenis_alat as $key => $value) :
                            $selected = $detail ? ($value->id === $detail->jenis_alat_id ? 'selected' : '') : '';
                            ?>
                            <option value="<?=$value->id?>" <?=$selected?>><?=$value->nama?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="alat_id">Alat</label>
                        <select name="alat_id" id="alat_id" class="form-control">
                            <option value="">Pilih Jenis Alat</option>
                            <?php foreach ($alat as $key => $value) :
                                $selected = $detail ? ($value->id === $detail->alat_id ? 'selected' : '') : '';
                            ?>
                            <option value="<?=$value->id?>" <?=$selected?>><?=$value->nama?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah Keluar</label>
                        <input type="text" class="form-control" name="jumlah" id="jumlah" value="<?=$detail ? $detail->jumlah : ''?>" placeholder="Masukkan nama">
                    </div>
                    <div class="form-group">
                        <label for="keperluan">Keperluan</label>
                        <textarea name="keperluan" id="keperluan" cols="30" rows="10" class="form-control"><?=$detail ? $detail->keperluan : ''?></textarea>
                    </div>
                        
                    
                </div>
                <!-- /.card-body -->

                <div class="card-footer float-right">
                    <a href="<?= base_url('admin/alat-keluar') ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i></a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('#jenis_alat').on('change', changeJenisAlat);

        function changeJenisAlat(callback){
            
            let jenis_alat_id = $('#jenis_alat option:selected').val();
            $('#alat_id').find('option').remove().end().append('<option value="">Pilih Jenis Alat</option>').val('');

            $.ajax({
                type: "get",
                url: "<?= base_url('admin/alat/ajax-get-alat-list') ?>",
                cache: false,
                data: {jenis_alat_id: jenis_alat_id},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(indx, val){
                        $('#alat_id').append($('<option>', {value:val.id, text:val.nama}));
                    });
                    callback();
                },
                error: function() {
                   alert('Something Wrong!');
                }
            });

        }

        <?php if(!empty($detail)){ ?>
            changeJenisAlat(function(){
                $('#alat_id').val(<?= $detail->alat_id ?>);
            });
        <?php } ?>
    });
</script>